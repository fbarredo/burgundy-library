const client = require('./connection.js')
const express = require('express');

const app = express();

app.listen(3300, ()=>{
    console.log("Sever is now listening at port 3300");
})

client.connect();

const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get('/api/users', (req, res)=>{
    client.query(`SELECT * FROM users`, (err, result)=>{
        if(!err){
            res.send(result.rows);
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "No users can be found"
            });
        }
    });
    client.end;
})

app.get('/api/users/:id', (req, res)=>{
    client.query(`SELECT * FROM users WHERE user_id = ${req.params.id}`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send(result.rows[0]);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "User can't be found"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    client.end;
})

app.post('/api/login', (req, res)=> {
    let user = req.body;

    client.query(`SELECT user_id, username, name FROM users WHERE username = '${user.username}' AND password = '${user.password}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send(result.rows[0]);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "User can't be found"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    
    client.end;
})

app.get('/api/books', (req, res)=>{
    client.query(`SELECT * FROM books`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send(result.rows);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "There are no books in the record"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    client.end;
})

app.get('/api/books/get', (req, res)=>{
    const isbn = req.query.isbn;

    client.query(`SELECT * FROM books WHERE isbn = '${isbn}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send(result.rows[0]);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "Book can't be found"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    client.end;
})

app.get('/api/search/books', (req, res)=>{
    const search = req.query.query;

    client.query(`SELECT * FROM books WHERE LOWER(isbn) LIKE LOWER('%${search}%') 
        OR LOWER(title) LIKE LOWER('%${search}%') OR LOWER(author) 
        LIKE LOWER('%${search}%') OR year = '${search}'`, (err, result)=>{

        if(!err && result.rowCount != 0){
            res.send(result.rows);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "Book can't be found"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    
    client.end;
})

app.delete('/api/books/delete', (req, res)=> {
    let book = req.body;

    let insertQuery = `DELETE FROM reviews where isbn = '${book.isbn}'`

    client.query(insertQuery, (err, result)=>{
        if(!err){
            let insertQuery = `DELETE FROM books where isbn = '${book.isbn}'`

            client.query(insertQuery, (err, result)=>{
                if(!err){
                    res.send({
                        'success': "Book has been deleted"
                    });
                }
                else{ 
                    console.log(err.message) 
                    res.send({
                        'error': "Error has been occurred"
                    });        
                }
            })
            client.end;
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });        
        }
    })
    client.end;
})

app.put('/api/books/update', (req, res)=> {
    let book = req.body;
    let updateQuery = `UPDATE books
                       SET title = '${book.title}',
                       author = '${book.author}',
                       year = '${book.year}'
                       WHERE isbn = '${book.isbn}'`

    client.query(updateQuery, (err, result)=>{
        if(!err){
            res.send({
                'success': "Book has been updated"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });        
        }
    })
    client.end;
})

app.post('/api/users/add', (req, res)=> {
    let user = req.body;
    isExisting = false;

    client.query(`SELECT * FROM users WHERE username = '${user.username}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send({
                'error': "User is existing"
            });
        } 
        else { 
            let insertQuery = `INSERT INTO users(name, username, password) 
                           VALUES('${user.name}', '${user.username}', '${user.password}')`
    
            client.query(insertQuery, (err, result)=>{
                if(!err){
                    res.send({
                        'success': "User is registered"
                    });
                }
                else{ 
                    console.log(err.message) 
                    res.send({
                        'error': "Error has been occurred"
                    });
                }
            })

            client.end;
        }
    });
    
    client.end;
})

app.post('/api/books/add', (req, res)=> {
    let book = req.body;
    isExisting = false;

    client.query(`SELECT * FROM books WHERE isbn = '${book.isbn}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send({
                'error': "Book is existing"
            });
        } 
        else { 
            let insertQuery = `INSERT INTO books(isbn, title, author, year) 
                           VALUES('${book.isbn}', '${book.title}', '${book.author}', '${book.year}')`
    
            client.query(insertQuery, (err, result)=>{
                if(!err){
                    res.send({
                        'success': "Book has been added"
                    });
                }
                else{ 
                    console.log(err.message) 
                    res.send({
                        'error': "Error has been occurred"
                    });
                }
            })

            client.end;
        }
    });
    
    client.end;
})

app.get('/api/reviews', function(req, res) {
    const isbn = req.query.isbn;

    client.query(`SELECT reviews.isbn, reviews.user_id, reviews.rating, reviews.review, users.name FROM reviews JOIN users ON reviews.user_id = users.user_id WHERE LOWER(isbn) = LOWER('${isbn}')`, (err, result)=>{

        if(!err && result.rowCount != 0){
            res.send(result.rows);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "There are no review"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    
    client.end;
})

app.post('/api/reviews/add', (req, res)=> {
    let review = req.body;

    client.query(`SELECT * FROM reviews WHERE isbn = '${review.isbn}' AND user_id = '${review.user_id}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send({
                'error': "Already submitted a review."
            });
        }
        else if(result.rowCount == 0){
            let insertQuery = `INSERT INTO reviews(isbn, user_id, rating, review) 
                VALUES('${review.isbn}', '${review.user_id}', '${review.rating}', '${review.review}')`
            
            client.query(insertQuery, (err, result)=>{
                if(!err){
                    res.send({
                        'success': "Added review"
                    });
                }
                else{ 
                    console.log(err.message) 
                    res.send({
                        'error': "Error has been occurred"
                    });
                }
            })

            client.end;
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    })
    client.end;
})


app.get('/api/reviews/stats', function(req, res) {
    const isbn = req.query.isbn;

    client.query(`SELECT AVG(rating)::numeric(10,2), COUNT(*) FROM reviews WHERE reviews.isbn = '${isbn}'`, (err, result)=>{

        if(!err && result.rowCount != 0){
            res.send(result.rows[0]);
        }
        else if(result.rowCount == 0){
            res.send({
                'error': "There are no review"
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    
    client.end;
})

app.get('/api/reviews/check', function(req, res) {
    const isbn = req.query.isbn;
    const user_id = req.query.userid;

    if(isNaN(user_id)) {
        res.send({
            'error': false
        });
    }
    else {
        client.query(`SELECT user_id FROM reviews WHERE user_id = '${user_id}' AND isbn = '${isbn}'`, (err, result)=>{
            if(!err && result.rowCount != 0){
                res.send({
                    'response': true
                });
            }
            else if(result.rowCount == 0){
                res.send({
                    'response': false
                });
            }
            else{ 
                console.log(err.message) 
                res.send({
                    'error': "Error has been occurred"
                });
            }
        });
    }
    
    client.end;
})

app.get('/api/book/check', function(req, res) {
    const isbn = req.query.isbn;

    client.query(`SELECT isbn, title FROM books WHERE isbn = '${isbn}'`, (err, result)=>{
        if(!err && result.rowCount != 0){
            res.send({
                'response': true
            });
        }
        else if(result.rowCount == 0){
            res.send({
                'response': false
            });
        }
        else{ 
            console.log(err.message) 
            res.send({
                'error': "Error has been occurred"
            });
        }
    });
    
    client.end;
})