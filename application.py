import os
import requests
import json

from flask import Flask, render_template, session, request, redirect, url_for, jsonify
from flask_session import Session

app = Flask(__name__)

if not os.getenv("API_URL"):
    raise RuntimeError("API_URL is not set")

api_url = os.getenv("API_URL")

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

defaultTitle = "Burgundy Library"

@app.route("/")
def index():
    user = None
    if 'user' in session:
        user = session['user']
    return render_template("index.html", title = defaultTitle, user=user)

@app.route("/login")
def login():
    if 'user' in session:
        return redirect(url_for('index'))
    return render_template("login.html", title = defaultTitle)

@app.route("/login/user", methods=["POST"])
def loginUser():
    user = None
    username = request.form["username"]
    password = request.form["password"]

    credentials = {'username' : username, 'password' : password}

    try:
        user = requests.post(api_url + "/api/login", json = credentials).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)
   
    if "error" in user:
        return render_template("login.html", message = "error")
    else:
        session['user'] = {"user_id": user["user_id"], "username": user["username"], "name": user["name"]}
        return redirect(url_for('catalog'))

    return redirect(url_for('login'))

@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for('login'))

@app.route("/register")
def register():
    if 'user' in session:
        return redirect(url_for('index'))
    return render_template("register.html", title = defaultTitle)

@app.route("/register/verify", methods=["POST"])
def verify():
    userExists = False
    
    name = request.form["fullname"]
    username = request.form["username"]
    password = request.form["password"]

    registerUser = {"name": name, "username": username}

    newCredentials = {'name' : name, 'username' : username, 'password' : password}

    try:
        loginAttempt = requests.post(api_url + "/api/users/add", json = newCredentials).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    if "error" in loginAttempt:
        return render_template("register.html", title = defaultTitle, registerUser = registerUser, error = loginAttempt["error"])
    else:
        return redirect(url_for('login'))

    return redirect(url_for('register'))

@app.route("/catalog", defaults={'success': None})
@app.route("/catalog/<string:success>")
def catalog(success=None):
    user = None
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    try:
        books = requests.get(api_url + "/api/books").json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    return render_template("catalog.html", title = defaultTitle, books = books, user = user, success = success)

@app.route("/catalog/search", methods=["GET"])
def search():
    user = None
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    query = request.args.get('query')
    if query == '':
        return render_template("catalog.html", title = defaultTitle, user = user)

    try:
        books = requests.get(api_url + "/api/search/books", params={"query": query}).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    return render_template("catalog.html", title = defaultTitle, books = books, user = user, query = query)

@app.route("/book/<string:isbn>", defaults={'success': None})
@app.route("/book/<string:isbn>/<success>")
def book(isbn, success=None):
    user = None
    ratings = None
    reviewed = True

    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    try:
        book = requests.get(api_url + "/api/books/get", params={"isbn": isbn}).json()
        
        reviews = requests.get(api_url + "/api/reviews", params={"isbn": isbn}).json() 

        rating_stats = requests.get(api_url + "/api/reviews/stats", params={"isbn": isbn}).json()

        reviewed = _checkReview(session.get("user")["user_id"], isbn)
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)


    return render_template("book.html", title = defaultTitle, book = book, user = user, reviews = reviews, rating_stats = rating_stats, reviewed = reviewed, success = success)

@app.route("/book/delete/<string:isbn>")
def deleteBook(isbn):
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    bookInfo = {'isbn' : isbn}

    try:
        bookDeleteAttempt = requests.delete(api_url + "/api/books/delete", json = bookInfo).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)
   
    if "success" in bookDeleteAttempt:
        return redirect(url_for('catalog', success = "delete"))
    else:
        return redirect(url_for('book', isbn=isbn))

    return redirect(url_for('book', isbn=isbn))

@app.route("/book/add")
def addBook():
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    return render_template("add.html", title = defaultTitle, user = user)

@app.route("/book/edit/<string:isbn>")
def editBook(isbn):
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    try:
        book = requests.get(api_url + "/api/books/get", params={"isbn": isbn}).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    return render_template("edit.html", title = defaultTitle, user = user, book = book)

@app.route("/book/add/submit", methods=["POST"])
def submitBook():
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    userExists = False

    isbn = request.form["isbn"]
    title = request.form["title"]
    author = request.form["author"]
    year = request.form["year"]

    book = {"isbn" : isbn, "title": title, "author": author, "year" : year}

    if _checkBook(isbn):
        return render_template("add.html", title = defaultTitle, user = user, book = book, error = "Book is Existing")
    else:

        try:
            addAttempt = requests.post(api_url + "/api/books/add", json = book).json()
        except requests.ConnectionError as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
        except requests.exceptions.Timeout as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
        except requests.exceptions.RequestException as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

        if "success" in addAttempt:
            return redirect(url_for('book', isbn = isbn, success = "add"))
        else:
            return render_template("add.html", title = defaultTitle, user = user, book = book, error = addAttempt["error"])

    return redirect(url_for('add'))
    
@app.route("/book/edit/submit/<string:isbn>", methods=["POST"])
def submitEditBook(isbn):
    if 'user' in session:
        user = session['user']
    else:
        return redirect(url_for('index'))

    userExists = False
    
    title = request.form["title"]
    author = request.form["author"]
    year = request.form["year"]

    book = {"isbn" : isbn, "title": title, "author": author, "year" : year}

    try:
        editAttempt = requests.put(api_url + "/api/books/update", json = book).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    if "success" in editAttempt:
        return redirect(url_for('book', isbn = isbn, success = "edit"))
    else:
        return redirect(url_for('editBook', isbn = isbn))

    return redirect(url_for('editBook', isbn = isbn))

@app.route("/book/review/<string:isbn>", methods=["POST"])
def submitReview(isbn):
    reviewExists = False
    user_id = session.get("user")["user_id"]
    rating = request.form["rating"]
    review = request.form["comment"]

    if _checkReview(user_id, isbn):
        return redirect(url_for('book', isbn = isbn, message = "error"))
    else:
        review = {"isbn" : isbn, "user_id": user_id, "rating": rating, "review" : review}

        try:
            reviewAttempt = requests.post(api_url + "/api/reviews/add", json = review).json()
        except requests.ConnectionError as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
        except requests.exceptions.Timeout as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
        except requests.exceptions.RequestException as e:
            session.clear()
            return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

        if "success" in reviewAttempt:
            return redirect(url_for('book', isbn = isbn))
        else:
            return redirect(url_for('book', isbn = isbn, success = 'false'))

    return redirect(url_for('book', isbn = isbn))

def _checkReview(user_id, isbn):
    try:
        reviewExists = requests.get(api_url + "/api/reviews/check", params={"isbn": isbn, "userid": user_id}).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)

    retVal = False

    print(reviewExists['response'])

    if reviewExists['response']:
        retVal = True
    else:
        retVal = False

    return retVal

def _checkBook(isbn):
    try:
        reviewExists = requests.get(api_url + "/api/book/check", params={"isbn": isbn}).json()
    except requests.ConnectionError as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Connection Error", errormsg = e)
    except requests.exceptions.Timeout as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Timeout", errormsg = e)
    except requests.exceptions.RequestException as e:
        session.clear()
        return render_template("login.html", title = defaultTitle, error = "Request Exception", errormsg = e)
        
    retVal = False

    print(reviewExists['response'])

    if reviewExists['response']:
        retVal = True
    else:
        retVal = False

    return retVal
    