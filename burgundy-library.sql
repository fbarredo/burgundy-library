PGDMP         4            	    y            burgundy_db    14.0    14.0                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16394    burgundy_db    DATABASE     o   CREATE DATABASE burgundy_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';
    DROP DATABASE burgundy_db;
                postgres    false            �            1259    16451    books    TABLE     �   CREATE TABLE public.books (
    isbn character varying(40) NOT NULL,
    title character varying(40) NOT NULL,
    author character varying(40) NOT NULL,
    year character varying(4)
);
    DROP TABLE public.books;
       public         heap    postgres    false            �            1259    16474    reviews    TABLE     �   CREATE TABLE public.reviews (
    review_id integer NOT NULL,
    isbn character varying(40) NOT NULL,
    user_id integer NOT NULL,
    review character varying(120),
    rating integer NOT NULL
);
    DROP TABLE public.reviews;
       public         heap    postgres    false            �            1259    16473    reviews_review_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reviews_review_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.reviews_review_id_seq;
       public          postgres    false    213                       0    0    reviews_review_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.reviews_review_id_seq OWNED BY public.reviews.review_id;
          public          postgres    false    212            �            1259    16403    users    TABLE     �   CREATE TABLE public.users (
    user_id integer NOT NULL,
    name character varying(80) NOT NULL,
    username character varying(40) NOT NULL,
    password character varying(40) NOT NULL
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16402    users_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.users_user_id_seq;
       public          postgres    false    210                       0    0    users_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;
          public          postgres    false    209            f           2604    16477    reviews review_id    DEFAULT     v   ALTER TABLE ONLY public.reviews ALTER COLUMN review_id SET DEFAULT nextval('public.reviews_review_id_seq'::regclass);
 @   ALTER TABLE public.reviews ALTER COLUMN review_id DROP DEFAULT;
       public          postgres    false    212    213    213            e           2604    16406    users user_id    DEFAULT     n   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    210    209    210            �          0    16451    books 
   TABLE DATA           :   COPY public.books (isbn, title, author, year) FROM stdin;
    public          postgres    false    211   �       �          0    16474    reviews 
   TABLE DATA           K   COPY public.reviews (review_id, isbn, user_id, review, rating) FROM stdin;
    public          postgres    false    213   �       �          0    16403    users 
   TABLE DATA           B   COPY public.users (user_id, name, username, password) FROM stdin;
    public          postgres    false    210                     0    0    reviews_review_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.reviews_review_id_seq', 17, true);
          public          postgres    false    212                       0    0    users_user_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.users_user_id_seq', 16, true);
          public          postgres    false    209            j           2606    16455    books books_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (isbn);
 :   ALTER TABLE ONLY public.books DROP CONSTRAINT books_pkey;
       public            postgres    false    211            l           2606    16479    reviews reviews_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (review_id);
 >   ALTER TABLE ONLY public.reviews DROP CONSTRAINT reviews_pkey;
       public            postgres    false    213            h           2606    16408    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    210            m           2606    16480    reviews reviews_isbn_fkey    FK CONSTRAINT     w   ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_isbn_fkey FOREIGN KEY (isbn) REFERENCES public.books(isbn);
 C   ALTER TABLE ONLY public.reviews DROP CONSTRAINT reviews_isbn_fkey;
       public          postgres    false    3178    211    213            n           2606    16485    reviews reviews_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);
 F   ALTER TABLE ONLY public.reviews DROP CONSTRAINT reviews_user_id_fkey;
       public          postgres    false    3176    210    213            �   �   x�e�AO� ���+�ݘJK��M4i41�����,��Q�t=z��7�=4(�2R�p������Dm��#�o�)���P��� <�O��������������[�m��i��q&>R\g~���!M�?y�����Ӻ�Z���~�}<�[�V�h%�������>P��i�d��V*�F^��`݅?��c�uo�S�\}Y+j��x�6���UU�����>6�aױ�[��/�+Xy      �   4   x�34�4�0024�025�44�t�/-RH���KI-V(J-�L-�4����� �1      �   2   x�34�tK�ɬP����SpJ�L-�EE�)��iIP�����)W� �e�     