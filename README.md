# WEB DEVELOPER - TECHNICAL EXAM 10/13/2021 for CTO

Web Application with Python, JavaScript and Web API Services

Burgundy Library Management System - an online library management system where you can create an account, login, and search, add, edit and delete books, you also write a review for each of the books.

Flask
Node.js
PostgreSQL

# Files & Directories
WEB APPLICATION <Python, Flask, HTML, CSS, JavaScript>

application.py - Main flask application.

books.csv - List of Books data.

requirements.txt - list of packages needed for the project that is provided by the course.

/templates/.. - Contains layouts/html files for the web application.

/static/.. - Contains assets for the web application such as photos, css and js.

WEB API <Node.js, Express, PostgreSQL>

api.js - Main API application

connection.js - Connection configuration for API

# Process of Deployment
1. Install python
2. Installing Flask and prerequisites

> pip install Flask

> pip install python-dotenv

3. Add path of Flask in the Environment Variables
4. Install Node.js
5. Run in the project directory in different terminal

> flask run

> node api.js